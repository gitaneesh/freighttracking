import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({providedIn:"root"})
export class ValidateService{

    constructor(){

    }

    private validateComponentEvent = new BehaviorSubject({});
    validateComponentEvent$ = this.validateComponentEvent.asObservable();    
    validateComponent(event: any) {
        this.validateComponentEvent.next(event);
    }

    private validateCompleteEvent = new BehaviorSubject({});
    validateCompleteEvent$ = this.validateCompleteEvent.asObservable();    
    validateCompleted(message: any) {
        this.validateCompleteEvent.next(message);
    }
}