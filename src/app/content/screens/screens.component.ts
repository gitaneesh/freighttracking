import { Component } from '@angular/core';

import { MENU_ITEMS } from '../../config/menu';

@Component({
  selector: 'app-pages',
  styleUrls: ['screens.component.scss'],
  templateUrl: 'screens.component.html'
})
export class ScreensComponent {

  menu = MENU_ITEMS;

}
