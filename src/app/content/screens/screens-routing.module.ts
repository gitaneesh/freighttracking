import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScreensComponent } from './screens.component';
const routes: Routes = [
  {
    path: '',
    component: ScreensComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./first/first.module')
        .then(m => m.FirstModule),
      },
    ]

  },
  {
    path: 'signin',
    loadChildren: () => import('./auth/auth.module')
    .then(m => m.AuthModule),
  },
  {
     path: 'signup',
     loadChildren: () => import('./auth/auth.module')
     .then(m => m.AuthModule),
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScreensRoutingModule {}
