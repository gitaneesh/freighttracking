import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {MatTabsModule} from '@angular/material/tabs';



import { FirstComponent } from './first.component';
import {OrigindestinationFreightComponent} from './components/origin-destination-freight/origin-destination-freight';
import { AddressComponent } from './components/origin-destination-freight/address/address.component';
import {FreightComponent} from './components/origin-destination-freight/freight/freight.component';
import {BillToComponent} from './components/billto/billto.component';
import {CustomerNotificationComponent} from './components/customernotification/customernotification';
import {NotesComponent} from './components/notes/notes.component';

import {InputTextComponent} from '../../partials/components/inputtext/inputtext.component';
import {DropDownComponent} from '../../partials/components/dropdown/dropdown.component';
import {ZIPCodeComponent} from '../../partials/components/zip-code/zip-code.component';
import {NavigationComponent} from '../../partials/components/navigation/navigation.component';
import {FirstRoutingModule} from '../first/first-routing-module';
import {AutopopulateInputTextComponent} from '../../partials/components/autopopulate-inputtext/autopopulate-inputtext.component'

import {EmailDirective} from '../../../core/directives/email.directive';
import {PhoneNumberDirective} from '../../../core/directives/phone.directive';
import {TimePipe} from '../../../core/pipes/timepipe';

import {ClientSearchService} from '../../../services/billtoClientSearch.service';


@NgModule({
imports: [
FirstRoutingModule,
CommonModule,
FormsModule,
MatTabsModule,
// RouterModule.forChild([
//     {
//     path: '',
//     component: FirstComponent
//     }
// ])
],
providers: [ClientSearchService],
entryComponents: [

],
declarations: [
  FirstComponent,
  OrigindestinationFreightComponent,
  AddressComponent,
  FreightComponent,
  InputTextComponent,
  DropDownComponent,
ZIPCodeComponent,
NavigationComponent,
  EmailDirective,
  PhoneNumberDirective,
  BillToComponent,
  CustomerNotificationComponent,
  NotesComponent,
  TimePipe,
  AutopopulateInputTextComponent
]
})
export class FirstModule {}
