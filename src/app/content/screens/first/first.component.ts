import {Component, OnInit, Input, HostBinding, OnDestroy,	Output,	ViewEncapsulation, ViewChild, ChangeDetectorRef} from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { Subject } from 'rxjs';
import {InputTextComponent} from '../../partials/components/inputtext/inputtext.component';
import {ValidateService} from '../../../services/validate.service';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FirstComponent implements OnInit, OnDestroy {
navLinks: any[];
activeLinkIndex = 1;
showPrevBtn: boolean;
@Input() action = '';
constructor(private router:Router, private validateService:ValidateService, private cdr: ChangeDetectorRef) {

  this.navLinks = [
    {
        label: 'Origin-Destination-Freight',
        link: '/origin-destination-freight',
        index: 0,
        isActive: false
    },
    {
        label: ' Bill To',
        link: '/billto',
        index: 1,
        isActive: false
    },
    {
        label: 'Customer Notification',
        link: '/customernotification',
        index: 2,
        isActive: false
    },
    {
      label: 'Notes',
      link: '/notes',
      index: 3,
      isActive: false
  },
];

}

ngOnInit(): void {
   this.router.events.subscribe((res) => {
          //     this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url.split('/')[2]));
         this.showPrevBtn = this.router.url.indexOf('/billto') === -1 ;
   });
  this.validateService.validateCompleteEvent$.subscribe(response => { if(response){ this.changeTabAfterValidation(response)}   } );
}

ngOnDestroy(): void {

}
navigationClickHandler(event)
{
  console.log(event);
  switch(event.type)
  {
    case 'prev':
    this.activeLinkIndex = this.activeLinkIndex - 1;
    break;
    case 'next':
    {
      this.validateService.validateComponent(event);

    }
    break;
  }
  this.cdr.detectChanges();

}

navigateTab() {
        this.navLinks[this.activeLinkIndex].isActive = false;
        switch (this.activeLinkIndex) {
          case 1:
          this.activeLinkIndex =      this.activeLinkIndex - 1;
          break;
          case 0:
          this.activeLinkIndex =      this.activeLinkIndex + 2;
          break;
          default:
          this.activeLinkIndex =      this.activeLinkIndex + 1;
        }
        this.navLinks[this.activeLinkIndex].isActive = true;
        const arrAnchor = document.getElementsByClassName('mat-tab-link');
        const elem = arrAnchor[this.activeLinkIndex] as HTMLElement;
        elem.click();
}

changeTabAfterValidation(response){
  console.log('changeTabAfterValidation',response);
  if (response.isValid) {
      this.navigateTab();
  }
}

}
