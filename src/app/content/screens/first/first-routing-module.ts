import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {OrigindestinationFreightComponent} from './components/origin-destination-freight/origin-destination-freight';
import {BillToComponent} from './components/billto/billto.component';
import {CustomerNotificationComponent} from './components/customernotification/customernotification';
import {NotesComponent} from './components/notes/notes.component';

import { FirstComponent } from './first.component';
const routes: Routes = [
  {
    path: '',
    component: FirstComponent,
    children: [
        { 
            path: '', 
            redirectTo: 'billto', 
            pathMatch: 'full' 
        },
        {
            path: 'origin-destination-freight',
            component: OrigindestinationFreightComponent
        },        
        {
            path: 'billto',
            component: BillToComponent
         },
        {
            path: 'customernotification',
            component: CustomerNotificationComponent
        },
        {
            path: 'notes',
            component: NotesComponent
        },
    ]

  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirstRoutingModule {}
