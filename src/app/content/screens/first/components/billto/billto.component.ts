import {Component, OnInit, Input, HostBinding, OnDestroy,	Output,	ViewEncapsulation, ViewChildren, QueryList} from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subject , Observable, of} from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/Operators';

import {InputTextComponent} from '../../../../partials/components/inputtext/inputtext.component';
import {DropDownComponent} from '../../../../partials/components/dropdown/dropdown.component';
import {ZIPCodeComponent} from '../../../../partials/components/zip-code/zip-code.component';
import {AutopopulateInputTextComponent} from '../../../../partials/components/autopopulate-inputtext/autopopulate-inputtext.component';
import {ValidateService} from '../../../../../services/validate.service';
import {ClientSearchService} from '../../../../../services/billtoClientSearch.service';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { catchError } from 'rxjs/internal/operators/catchError';
import { Subscription } from 'rxjs/internal/Subscription';
@Component({
  selector: 'app-billto',
  templateUrl: './billto.component.html',
  styleUrls: ['./billto.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BillToComponent implements OnInit, OnDestroy {
  isValid:Boolean = false;

  @ViewChildren(InputTextComponent) inputTextComponent: QueryList<InputTextComponent>;
  @ViewChildren(DropDownComponent) dropDownComponent: QueryList<DropDownComponent>;
  // @ViewChildren(ZIPCodeComponent) zipCodeComponent: QueryList<ZIPCodeComponent>;
  @ViewChildren(AutopopulateInputTextComponent) autopopulateInputText: QueryList<AutopopulateInputTextComponent>;

  ddBillToOptions: any[];
  private searchTerms = new Subject<string>();
  ddOptions:any;
  selectedClient: any;
  private validateSubscription: Subscription;
constructor(private validateService: ValidateService, private clientSearchService: ClientSearchService  ) {
   console.log('BillToComponent');

}

ngOnInit(): void {
  this.validateSubscription = this.validateService.validateComponentEvent$.subscribe(event => this.validateChildComponent(event));
  this.searchTerms.pipe(
                                 debounceTime(250),
                                 distinctUntilChanged(),
                                 switchMap(term => term ? this.clientSearchService.search(term) : of<any[]>([]))).subscribe((response)=>{
                                   console.log(response);
                                   this.ddBillToOptions =  response;
                                 }) ;

}
ngOnDestroy(): void {
  this.validateSubscription.unsubscribe();
}

validateChildComponent(event){
  if(event && event.type == 'next')
    {
      if(this.inputTextComponent){
          for(let inputText of this.inputTextComponent.toArray())
          {
            this.isValid = inputText.validateInputElement();
          }
      }
      if(this.dropDownComponent){
        for(let dropdown of this.dropDownComponent.toArray())
        {
          this.isValid = dropdown.validateInputElement();
        }
    }
    // if(this.zipCodeComponent){
    //   for(let zipCode of this.zipCodeComponent.toArray())
    //   {
    //     this.isValid = zipCode.validateInputElement();
    //   }
    // }
  if(this.autopopulateInputText){
      for(let autopopulate of this.autopopulateInputText.toArray())
      {
        this.isValid = autopopulate.validateInputElement();
      }
    }
      console.log('isValid', this.isValid)
      if(this.isValid)
        {
          this.validateService.validateCompleted({isValid:this.isValid})
        }
    }
  }
  zipCodeChangeHandler(event){
    console.log('Zip Code ',event);
  }

   ddBillToChangeHandler(event) {
        console.log('ddBillToChangeHandler  ', event);
        this.selectedClient = event;
      }
    serchTextChangehandler(event) {
      this.searchTerms.next(event);
    }

}
