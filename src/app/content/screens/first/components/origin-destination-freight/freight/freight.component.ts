import {Component, OnInit, Input, HostBinding, OnDestroy,	Output,	ViewEncapsulation,ViewChildren, QueryList} from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import {InputTextComponent} from '../../../../../partials/components/inputtext/inputtext.component';

@Component({
  selector: 'app-freight',
  templateUrl: './freight.component.html',
  styleUrls: ['./freight.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FreightComponent implements OnInit, OnDestroy {
private isValid:Boolean = false;
public isSubmit:Boolean = false;
@ViewChildren(InputTextComponent) inputTextChildren :QueryList<InputTextComponent>;
@Input() action = '';

constructor() {
   console.log('Freight Component');

}

ngOnInit(): void {

}
ngOnDestroy(): void {

}

validateInputElement(){ 
  if(this.inputTextChildren){
      for(let inputText of this.inputTextChildren.toArray())
      {
        this.isValid = inputText.validateInputElement();
      }          
  }
  return this.isValid;
}


}
