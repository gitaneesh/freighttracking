import {Component, OnInit, Input, HostBinding, OnDestroy,	Output,	ViewEncapsulation, ViewChildren, QueryList, ViewChild} from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import {AddressComponent} from './address/address.component';
import {FreightComponent} from './freight/freight.component';
import {ValidateService} from '../../../../../services/validate.service';
import { Subscription } from 'rxjs/internal/Subscription';
@Component({
  selector: 'app-orig-dest-freight',
  templateUrl: './origin-destination-freight.html',
  styleUrls: ['./origin-destination-freight.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OrigindestinationFreightComponent implements OnInit, OnDestroy {
private isValid:Boolean = false;
private validateSubscription: Subscription;
@ViewChildren(AddressComponent) addressComponent : QueryList<AddressComponent>;
//Note change the below ViewChildren to ViewChild
@ViewChildren(FreightComponent) freightComponent : QueryList<FreightComponent>;

constructor(private validateService: ValidateService) {
   console.log('OrigindestinationFreightComponent');

}

ngOnInit(): void {
 this.validateSubscription = this.validateService.validateComponentEvent$.subscribe(event => this.validateChildComponent(event))
}
ngOnDestroy(): void {
  this.validateSubscription.unsubscribe();
}

validateChildComponent(event){
  if(event && event.type == 'next')
  {
    if(this.addressComponent){
        for(let address of this.addressComponent.toArray())
        {
          this.isValid = address.validateInputElement();
        }
    }
    if(this.freightComponent){
        for(let freight of this.freightComponent.toArray())
        {
          this.isValid = freight.validateInputElement();
        }
    }
    console.log('isValid', this.isValid)
    if(this.isValid)
      {
        this.validateService.validateCompleted({isValid:this.isValid})
      }
  }
}

}
