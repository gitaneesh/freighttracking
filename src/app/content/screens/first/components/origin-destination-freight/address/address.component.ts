import {Component, OnInit, Input, HostBinding, OnDestroy,	Output,	ViewEncapsulation, ViewChildren, QueryList} from '@angular/core';
import { Subject } from 'rxjs';
//import {PhoneNumberPipe} from '../../../../../../core/pipes/phone-number.pipe';
import {InputTextComponent} from '../../../../../partials/components/inputtext/inputtext.component';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddressComponent implements OnInit, OnDestroy {
  @ViewChildren(InputTextComponent) inputTextChildren :QueryList<InputTextComponent>;

  @Input() title: string;
  @Input() label: string;
  @Input() type: string;
  
  ddOptions: any[] = [{value: 'HS', label: 'HS'},
                      {value: 'MS', label: 'MS'}];
  
  public Shipper:any;
  public selectedTimeFormat:any;
  public selectedAirPort:any;

  public isSubmit:Boolean = false;
  private isValid:Boolean = false;
  
  constructor() {
  }

ngOnInit(): void {
}
ngOnDestroy(): void {

}
airportCodeChangehandler(event) {
  
}

TimeCodeChangehandler(event) {
    this.selectedTimeFormat = event.value;
    console.log(' this.selectedTimeFormat ', this.selectedTimeFormat);
 }

validateInputElement(){ 
  if(this.inputTextChildren){        
    for(let inputText of this.inputTextChildren.toArray())
    {
      this.isValid = inputText.validateInputElement();
    }        
  }
  return this.isValid;
}



}
