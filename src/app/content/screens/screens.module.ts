import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScreensRoutingModule } from './screens-routing.module';
//import {PhoneNumberPipe} from '../../core/pipes/phone-number.pipe';
import {
  AppLayoutComponent,
  AsideComponent,
  FooterComponent,
  HeaderComponent,
  SubHeaderComponent } from '../layout';
import {ScreensComponent} from './screens.component';
const COMPONENTS = [
  ScreensComponent,
  AppLayoutComponent,
  AsideComponent,
  FooterComponent,
  HeaderComponent,
  SubHeaderComponent
];

@NgModule({
  imports: [
    CommonModule,
    ScreensRoutingModule,
    //PhoneNumberPipe,
  ],
  declarations: [COMPONENTS],

})
export class ScreensModule { }
