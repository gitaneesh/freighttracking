import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.scss'],
   templateUrl: './header.html'
})
export class HeaderComponent {}
