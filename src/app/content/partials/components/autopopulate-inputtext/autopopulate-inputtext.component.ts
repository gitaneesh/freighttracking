import {
  Component,
  OnInit,
  Input,
  HostBinding,
  OnDestroy,
  Output,
  ViewEncapsulation,
  HostListener,
  ViewChild,
  OnChanges,
  EventEmitter} from '@angular/core';
import { Element } from '@angular/compiler';
import {ValidateService} from '../../../../services/validate.service';




@Component({
  selector: 'app-autopopulate-input-text',
  styleUrls: ['autopopulate-inputtext.component.scss'],
  templateUrl: 'autopopulate-inputtext.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AutopopulateInputTextComponent implements OnInit, OnChanges {

  @Input() options: Array <any>;
  @Input() class?: string;
  @Input() placeHolder?: string;
  @Input() width?: string;
  @Input() required?: boolean;
  @Input() submitted?: boolean;
  @Input() minlength?: number;
  @Input() maxlength?: number;
  @Output() serchText: EventEmitter <string> = new EventEmitter();
  @Output() selection: EventEmitter <any> = new EventEmitter();
  @ViewChild('ipElement', {static: false}) ipEl: any;

  private currentFocus;

  constructor() {
    console.log('Autopopulate Input text  component');
  }

  ngOnInit() {
    const owner: any = this;
    document.addEventListener('click', (e) => {  owner.closeAllLists(); } );
  }
  @HostListener('input', ['$event.target.value'])
  onInput(value) {
      let a: any ;
      let b: any;
      let i: any;
      let val: any = value;
      this.closeAllLists();
      if (!val) { return false; }
      this.serchText.emit(val);
  }
  @HostListener('keydown', ['$event'])
  onKeyDown(event) {
    let elem =  event.target;
    let value = elem.value;
    let x: any = document.getElementById("autocomplete-list");
    if (x) { x = x.getElementsByTagName('div'); }
    if (event.keyCode == 40) {
      this.currentFocus++;
      this.addActive(x);
    } else if (event.keyCode == 38) { //up
      this.currentFocus--;
      this.addActive(x);
    } else if (event.keyCode == 13) {
      event.preventDefault();
      if (this.currentFocus > -1) {
        if (x){x[this.currentFocus].click();}
      }
    }
  }
  private addActive(x) {
    if (!x){return false;}
    this.removeActive(x);
    if (this.currentFocus >= x.length){ this.currentFocus = 0;}
    if (this.currentFocus < 0){ this.currentFocus = (x.length - 1);}
    x[this.currentFocus].classList.add('autocomplete-active');
  }
  private removeActive(x) {
    for (let i = 0; i < x.length; i++) {
      x[i].classList.remove('autocomplete-active');
    }
  }
  private closeAllLists() {
    let x = document.getElementsByClassName('autocomplete-items');
    for (var i = 0; i < x.length; i++) {
       x[i].parentNode.removeChild(x[i]);
    }
    this.options = [];

  }
  public focusOutFunction(event) {
    let value: any = event.target.value;
    const required = event.target.required;
    if (value.length > 0) {
      this.highlight('#8f9bb3');
      return true;
   } else {
           if(required){  this.highlight('#fa0404');}
             return false;
           }
  }

  private highlight(color: string) {
      this.ipEl.nativeElement.style.borderColor = color;
  }
  optionSelecthandler(event,option) {
    this.ipEl.nativeElement.value = option.label;
    this.selection.emit(option);
  }

  ngOnChanges(changes: any) {
        console.log('changes  ',changes);
   }

   validateInputElement(){
    if(this.ipEl.nativeElement.required && this.ipEl.nativeElement.value == ''){
      this.highlight('#fa0404');
      return false;
    }
    else{
      return true
    }
  }


}
