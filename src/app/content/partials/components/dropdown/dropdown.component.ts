import {Component, OnInit, Input, HostBinding, OnDestroy,	Output,	ViewEncapsulation, HostListener, ViewChild, EventEmitter} from '@angular/core';
import { Element } from '@angular/compiler';



@Component({
  selector: 'app-dropdown',
  styleUrls: ['dropdown.component.scss'],
  templateUrl: 'dropdown.component.html',
  encapsulation: ViewEncapsulation.None
})
export class DropDownComponent {

  @Input() required?: boolean;
  @Input() options: any[] ;
  @Output() selection: EventEmitter<any> = new EventEmitter();

  @ViewChild('ddElement', {static: false}) ddEl: any;

  constructor() {
    console.log('DropDown  component');
  }

  changeOptions(event) {
        console.log('event    :', event.target.value );
        this.selection.emit({value: event.target.value});
  }

  @HostListener('input', ['$event.target.value'])
    onInput(value) {
      if (value != 'select')
      {
        this.highlight('#8f9bb3');
        return true;
      } 
      else { 
        this.highlight('#fa0404');
        return false;
     }
    }
  
    private highlight(color: string) {
      this.ddEl.nativeElement.style.borderColor = color;
  }
  
  validateInputElement(){        
    console.log('dropdown validation', this.ddEl.nativeElement.required)
    if(this.ddEl.nativeElement.required && this.ddEl.nativeElement.value == 'select'){          
      this.highlight('#fa0404');
      return false;
    }
    else{
      return true
    }    
  }

}
