import {Component, OnInit, Input, HostBinding, OnDestroy,	Output,	ViewEncapsulation, HostListener,
   ViewChild, EventEmitter} from '@angular/core';
import { Element } from '@angular/compiler';




@Component({
  selector: 'app-navigation',
  styleUrls: ['navigation.component.scss'],
  templateUrl: 'navigation.component.html',
  encapsulation: ViewEncapsulation.None
})
export class NavigationComponent {

  @Input() index: number ;
  @Input() total: number ;
  @Input() showPrev: boolean ;
  @Output() clickEvent: EventEmitter<any> = new EventEmitter();

  constructor() {
    console.log('NavigationComponent');
  }
  prevBtnClickHandler(event) {
    this.clickEvent.emit({type: 'prev'});
  }
  nextBtnClickHandler(event) {
    this.clickEvent.emit({type: 'next'});
  }

}
