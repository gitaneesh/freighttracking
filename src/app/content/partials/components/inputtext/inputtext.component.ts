import {
  Component,
  OnInit,
  Input,
  HostBinding,
  OnDestroy,
  Output,
  ViewEncapsulation,
  HostListener,
  ViewChild,
  OnChanges,
  EventEmitter} from '@angular/core';
import { Element } from '@angular/compiler';
import {ValidateService} from '../../../../services/validate.service';




@Component({
  selector: 'app-input-text',
  styleUrls: ['inputtext.component.scss'],
  templateUrl: 'inputtext.component.html',
  encapsulation: ViewEncapsulation.None
})
export class InputTextComponent implements OnInit{

  @Input() type: string; // text,email,mobile,number,zip
  @Input() model: string;
  @Input() pipe?: string;
  @Input() class?: string;
  @Input() placeHolder?: string;
  @Input() width?: string;
  @Input() required?: boolean;
  @Input() readonly?: boolean;
  @Input() submitted?: boolean;
  @Input() minlength?: number;
  @Input() maxlength?: number;
  @Input() value?: string;
  @ViewChild('ipElement', {static: false}) ipEl: any;


  private mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  private mobileFormat =  /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  private zipCodeFormat = /^\d{5}$|^\d{5}-\d{4}$/;


  constructor(private validateService:ValidateService) {
    console.log('Input text  component');
  }

  ngOnInit(){
    //this.validateService.validateComponentEvent.subscribe(event => this.validateInputElement(event))
  }

  @HostListener('input', ['$event.target.value'])
  onInput(value) {
     switch (this.type) {
        case 'text':
          if (value.length > 0) {
               this.highlight('#8f9bb3');
               return true;
            } else { this.highlight('#fa0404');
                      return false;
                    }
        break;
        case 'email':
          if (value.match(this.mailformat)) {
                 this.highlight('#8f9bb3');
                 return true;
          } else {
            this.highlight('#fa0404');
            return false;
          }
        break;
        case 'mobile':
          if (value.length === 10) {
              if (value.match(this.mobileFormat)) {
                   this.ipEl.nativeElement.value  = this.mobileFormatting(value);
                   this.highlight('#8f9bb3');
                   return true;
                  } else {
                    this.highlight('#fa0404');
                    return false;
              }
          }
        break;
        case 'zip':
        if (value.match(this.zipCodeFormat)) {
               this.highlight('#8f9bb3');
               return true;
        } else {
          this.highlight('#fa0404');
          return false;
        }
      break;
   }
  }
  public focusOutFunction(event) {
    let value = event.target.value;
    const required = event.target.required;
    switch (this.type) {
      case 'text':
        if (value.length > 0) {
             this.highlight('#8f9bb3');
             return true;
          } else {
                  if(required){  this.highlight('#fa0404');}
                    return false;
                  }
      break;
      case 'email':
        if (value.match(this.mailformat)) {
               this.highlight('#8f9bb3');
               return true;
        } else {
          if(required){  this.highlight('#fa0404');}
          return false;
        }
      break;
      case 'mobile':
        if (value.match(this.mobileFormat)) {
           this.highlight('#8f9bb3');
           this.ipEl.nativeElement.value  = this.mobileFormatting(value);
           return true;
        } else {
          if (required) {  this.highlight('#fa0404');}
           return false;
        }
      break;
      case 'zip':
      if (value.match(this.zipCodeFormat)) {
             this.highlight('#8f9bb3');
             return true;
      } else {
        if (required) {  this.highlight('#fa0404');}
        return false;
      }
    break;
    }

  }

  private highlight(color: string) {
      this.ipEl.nativeElement.style.borderColor = color;
  }

  ngOnChanges(changes:any)
  {
    // console.log('ngOnChange', changes[`submitted`][`currentValue`]);
    // if (changes.hasOwnProperty(`submitted`) && changes[`submitted`][`currentValue`] != undefined &&  changes[`submitted`][`currentValue`] == true ){
    //       if(this.ipEl.nativeElement.type == 'text' && this.ipEl.nativeElement.required && this.ipEl.nativeElement.value == ''){
    //       this.highlight('#fa0404');
    //       this.validateService.validateCompleted({isValid:false})
    //     }
    // }
  }

  validateInputElement(){
    if(this.ipEl.nativeElement.type == 'text' && this.ipEl.nativeElement.required && this.ipEl.nativeElement.value == ''){
      this.highlight('#fa0404');
      return false;
    }
    else{
      return true
    }
  }

  // @Output() isChildConponentValid : EventEmitter<any> = new EventEmitter<any>()

  // public validateChildComponent(data){
  //   console.log('validateChildComponent', data)
  //   return this.onInput(data) ? this.isChildConponentValid.emit(true) : this.isChildConponentValid.emit(false)

  // }
  private mobileFormatting(tel) {
      if (!tel) { return; }
      const value = tel.toString().trim().replace(/^\+/, '');
      if (value.match(/[^0-9]/)) {
          return tel;
      }
      console.log('value  :', value);
      let country, city, number;

      switch (value.length) {
          case 10: // +1PPP####### -> C (PPP) ###-####
              country = 1;
              city = value.slice(0, 3);
              number = value.slice(3);
              break;

          case 11: // +CPPP####### -> CCC (PP) ###-####
              country = value[0];
              city = value.slice(1, 4);
              number = value.slice(4);
              break;

          case 12: // +CCCPP####### -> CCC (PP) ###-####
              country = value.slice(0, 3);
              city = value.slice(3, 5);
              number = value.slice(5);
              break;

          default:
              return tel;
      }

      if (country === 1) {
          country = '';
      }

      number = number.slice(0, 3) + '-' + number.slice(3);
      console.log((country + " (" + city + ") " + number).trim());
      return (country + " (" + city + ") " + number).trim();

  }

}
